#!/bin/sh

set -x

# Now ensure that pex can't download anything from PyPI.
export http_proxy=127.0.0.1:9
export https_proxy=127.0.0.1:9

pex --python=python3 -m textwrap -vv -o script && ./script

if [ $? -eq 0 ]; then
    echo 9
fi

echo -1
